package com.test;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 * Created by neeraj on 03/11/16.
 */
public class DownloadFile {


    public static void main(String...args) throws Exception{
//        download("http://rbidocs.rbi.org.in/rdocs/Content/DOCs/IFCB2009_105.xls", "/Users/neeraj/work/scripts");
        downloadFileFromURL("http://rbidocs.rbi.org.in/rdocs/Content/DOCs/IFCB2009_105.xls");
    }

    public static Path download(String sourceUrl,
                                String targetDirectory) throws MalformedURLException, IOException
    {
        URL url = new URL(sourceUrl);

        String fileName = url.getFile();

        Path targetPath = new File(targetDirectory + fileName).toPath();

        Files.copy(url.openStream(), targetPath,
                StandardCopyOption.REPLACE_EXISTING);

        return targetPath;
    }

    public static void downloadFileFromURL(String urlString) {
        try {
            File destination = new File("/Users/neeraj/work/scripts/test.xls");
            URL website = new URL(urlString);
            URLConnection connection = (website.openConnection());

            HSSFWorkbook wb = new HSSFWorkbook(connection.getInputStream());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
