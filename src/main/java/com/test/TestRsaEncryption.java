package com.test;


import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

/**
 * TestRsaEncryption
 * Created by neeraj on 23/07/16.
 */
public class TestRsaEncryption {

    public static void main(String...a) throws Exception{
        TestRsaEncryption t = new TestRsaEncryption();
        t.init();
        System.out.println("bfnps6109H".matches("[A-Za-z]{3}[Pp][A-Za-z]\\d{4}[A-Za-z]{1}"));
        System.out.println(t.encrypt("2734694"));
        System.out.println(t.encrypt("2734863"));

    }

    private static final String DEFAULT_KEY_XML = "<RSAKeyValue><Modulus>68CObYbcjNqXUL7v8qZu1g+Oamh1197Fo02XhhzUpkIXvahzMvwnrKrnfhOGXv9WIuvRrXh7fHz7DtjMj/xx+Q==</Modulus><Exponent>AQAB</Exponent><P>9yvKj5U9EmjcapWs9UnDeYOgdgkA4ALPmJkZ12SFsIU=</P><Q>9CxY5sO3T6iJLWA9H+iJ4Ce/CEhCcXxYL7A/LYUiz+U=</Q><DP>cFopg0bVMe8UciaDLiRPhIa+g6joCut8LeM6CdyZoHk=</DP><DQ>co0joA+NvmdZA9q0knWryWecLayIz2kAjk7nNNnS/Sk=</DQ><InverseQ>rFQ+P6U+WPh+lAPJJzSwiYX9KxsAJPHNldiF7HUdBzg=</InverseQ><D>TlGt/1zqMDD+S+jite3srFiGq3sAizKK3fGNARTa4FA5b5B6n8QtNaQVA9sgqgSHJwALQXq39F01fL8yiGMiwQ==</D></RSAKeyValue>";
    private static final String MODULUS = "68CObYbcjNqXUL7v8qZu1g+Oamh1197Fo02XhhzUpkIXvahzMvwnrKrnfhOGXv9WIuvRrXh7fHz7DtjMj/xx+Q==";
    private static final String EXPONENT = "AQAB";
    private static final String DEFAULT_KEY = "<RSAKeyValue><P>9yvKj5U9EmjcapWs9UnDeYOgdgkA4ALPmJkZ12SFsIU=</P><Q>9CxY5sO3T6iJLWA9H+iJ4Ce/CEhCcXxYL7A/LYUiz+U=</Q><DP>cFopg0bVMe8UciaDLiRPhIa+g6joCut8LeM6CdyZoHk=</DP><DQ>co0joA+NvmdZA9q0knWryWecLayIz2kAjk7nNNnS/Sk=</DQ><InverseQ>rFQ+P6U+WPh+lAPJJzSwiYX9KxsAJPHNldiF7HUdBzg=</InverseQ><D>TlGt/1zqMDD+S+jite3srFiGq3sAizKK3fGNARTa4FA5b5B6n8QtNaQVA9sgqgSHJwALQXq39F01fL8yiGMiwQ==</D></RSAKeyValue>";
    public static final int DEFAULT_KEY_SIZE = 512;


    private static Cipher cipher = null;

    public String encrypt(String data){
        String s = doEncrypt(data);
        System.out.println("Encrypted {}"+ s);

        try {
            String escapeHtml1 = URLEncoder.encode(s, "UTF-8");
            System.out.println("Encrypted  escapeHtml 1: {}" + escapeHtml1);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return s;
    }
    


    private String doEncrypt(String data) {
        System.out.println("ENCRYPTING " + data + " - " +  data.length());
        String result = data;
        try {
            byte[] encryptedBytes = blockCipher(data.getBytes(), Cipher.ENCRYPT_MODE);
            result = new String(Base64.getEncoder().encode(encryptedBytes));
        } catch (Exception e) {
            System.out.println("An error occurred during the encryption!");
            e.printStackTrace();
        }
        return result;
    }



    private void init() {
        PublicKey publicKey = initPublicKey();
        initCipher(publicKey);
    }

    private PublicKey initPublicKey() {
        PublicKey publicKey = null;
        try {
            RSAPublicKeySpec rsaPubKey = getRsaPublicKeySpec();
            KeyFactory fact = KeyFactory.getInstance("RSA");
            publicKey = fact.generatePublic(rsaPubKey);

        } catch (Exception e) {
            System.out.println("Algorithm not supported! " + e.getMessage());
        }

        return publicKey;
    }

    private RSAPublicKeySpec getRsaPublicKeySpec() {
        byte[] modulusBytes = Base64.getDecoder().decode(MODULUS);
        byte[] exponentBytes = Base64.getDecoder().decode(EXPONENT);

        BigInteger modulus = new BigInteger(1, modulusBytes);
        BigInteger publicExponent = new BigInteger(1, exponentBytes);

        return new RSAPublicKeySpec(modulus, publicExponent);
    }

    private void initCipher(PublicKey publicKey) {
        try {
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);            // ENCRYPT using the PUBLIC key
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Algorithm not supported! " + e.getMessage() + "!");
        } catch (Exception e) {
            System.out.println("Cipher cannot be created!");
            e.printStackTrace();
        }
    }


    private byte[] blockCipher(byte[] bytes, int mode) throws IllegalBlockSizeException, BadPaddingException{
        // string initialize 2 buffers.
        // scrambled will hold intermediate results
        byte[] scrambled = new byte[0];

        // toReturn will hold the total result
        byte[] toReturn = new byte[0];
        // if we encrypt we use 100 byte long blocks. Decryption requires 128 byte long blocks (because of RSA)
        int length = Math.min(((mode == Cipher.ENCRYPT_MODE)? 50 : 64), bytes.length);

        // another buffer. this one will hold the bytes that have to be modified in this step
        byte[] buffer = new byte[length];
        for (int i=0; i< bytes.length; i++){

            // if we filled our buffer array we have our block ready for de- or encryption
            if ((i > 0) && (i % length == 0)){
                //execute the operation
                scrambled = cipher.doFinal(buffer);
                // add the result to our total result.
                toReturn = append(toReturn,scrambled);
                // here we calculate the length of the next buffer required
                int newlength = length;

                // if newlength would be longer than remaining bytes in the bytes array we shorten it.
                if (i + length > bytes.length) {
                    newlength = bytes.length - i;
                }
                // clean the buffer array
                buffer = new byte[newlength];
            }
            // copy byte into our buffer.
            buffer[i%length] = bytes[i];
        }

        // this step is needed if we had a trailing buffer. should only happen when encrypting.
        // example: we encrypt 110 bytes. 100 bytes per run means we "forgot" the last 10 bytes.
        // they are in the buffer array
        scrambled = cipher.doFinal(buffer);

        // final step before we can return the modified data.
        toReturn = append(toReturn,scrambled);

        return toReturn;
    }

    private byte[] append(byte[] prefix, byte[] suffix){
        byte[] toReturn = new byte[prefix.length + suffix.length];
        System.arraycopy(prefix, 0, toReturn, 0, prefix.length);
        System.arraycopy(suffix, 0, toReturn, prefix.length, suffix.length);
        return toReturn;
    }

}
