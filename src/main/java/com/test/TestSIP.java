package com.test;

import java.text.DecimalFormat;

/**
 * Created by neeraj on 23/09/16.
 */
public class TestSIP {

    public static void main(String...args){
        double amount = 10000.0; // SIP amount
        double rate = 10.0/1200.0;   // monthly return
        double timeInMonths = 12.0 * 5 ; // time frame

        DecimalFormat df = new DecimalFormat("#.##");
        System.out.println( amount + " = " + df.format( amount * sipFactor(rate, timeInMonths)));
        System.out.println( amount + " = " + df.format( amount * sipFactor(rate, timeInMonths)));
        System.out.println( amount + " = " + df.format( amount * sipFactor(rate, timeInMonths)));
    }

    private static double sipFactor(double rate, double time) {

        final double numerator = Math.pow((rate + 1), time);
        final double denominator = Math.pow((rate + 1), -1);

        return  (numerator - 1) / (1 - denominator);
    }

}
