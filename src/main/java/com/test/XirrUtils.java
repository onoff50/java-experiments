package com.test;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * XirrUtils
 * http://stackoverflow.com/questions/36789967/java-program-to-calculate-xirr-without-using-excel-or-any-other-library
 * Created by neeraj on 30/03/17.
 */


public class XirrUtils {

    private static final double tol = 0.001;

    private static double daysBetween(Date d1, Date d2) {
        long day = 24 * 60 * 60 * 1000;

        return (d1.getTime() - d2.getTime()) / day;
    }

    private static double fXirr(double p, Date dt, Date dt0, double x) {
        return p * Math.pow((1.0 + x), (daysBetween(dt0, dt) / 365.0));
    }

    private static double dfXirr(double p, Date dt, Date dt0, double x) {
        return (1.0 / 365.0) * daysBetween(dt0, dt) * p * Math.pow((x + 1.0), ((daysBetween(dt0, dt) / 365.0) - 1.0));
    }

    private static double totalFXirr(List<Double> payments, List<Date> days, double x) {
        double resf = 0.0;

        for (int i = 0; i < payments.size(); i++) {
            resf = resf + fXirr(payments.get(i), days.get(i), days.get(0), x);
        }

        return resf;
    }

    private static double totalDfXirr(List<Double> payments, List<Date> days, double x) {
        double resf = 0.0;

        for (int i = 0; i < payments.size(); i++) {
            resf = resf + dfXirr(payments.get(i), days.get(i), days.get(0), x);
        }

        return resf;
    }

    private static double newtonsMethod(double guess, List<Double> payments, List<Date> days) {
        double x0 = guess;
        double x1;
        double err = 1e+100;

        while (err > tol) {
            x1 = x0 - totalFXirr(payments, days, x0) / totalDfXirr(payments, days, x0);
            err = Math.abs(x1 - x0);
            x0 = x1;
        }

        return x0;
    }


    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public static Date strToDate(String str) {
        try {
            return sdf.parse(str);
        } catch (ParseException ex) {
            return null;
        }
    }

    public static double xirr(List<Double> payments, List<Date> days) {
        double guess = 0.1;
        double xirr = newtonsMethod(guess, payments, days);
        System.out.println("XIRR value is " + xirr);
        return xirr;
    }

    public static void main(String... agrs) {
        List<Double> xAmountList = new LinkedList<Double>();
        List<Date> xDateList = new LinkedList<Date>();

        xAmountList.add(-500.0);
        xAmountList.add(-5000.0);
        xAmountList.add(-20000.0);
        xAmountList.add(-5000.0);
        xAmountList.add(-2500.0);

        xAmountList.add(21000.0);
        xAmountList.add(2000.0);
        xAmountList.add(10251.3);

        xDateList.add(strToDate("19/09/2016"));
        xDateList.add(strToDate("30/03/2017"));
        xDateList.add(strToDate("31/10/2016"));
        xDateList.add(strToDate("03/02/2017"));
        xDateList.add(strToDate("04/10/2016"));
        xDateList.add(strToDate("14/11/2016"));
        xDateList.add(strToDate("06/02/2017"));
        xDateList.add(strToDate("04/04/2017"));

        System.out.print(xirr(xAmountList, xDateList));
    }
}
