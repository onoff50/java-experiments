package com.test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;

/**
 * Created by neeraj on 31/08/16.
 */
public class TestIncome {

    static long LAKH = 100000;
    private static final DecimalFormat fourDForm = new DecimalFormat("#.######");


    public static void main(String... args) {


        System.out.println("1000, 100 LCM = " + lcmcal(1000, 100));
        System.out.println("1000, 100 LCM = " + lcmcal(1000, 10));
        System.out.println("1000, 100 LCM = " + lcmcal(100, 50));
        System.out.println("1000, 100 LCM = " + lcmcal(50, 20));
//        System.out.println("2000000 = " + getAnnualIncomeText(2000000));
//        System.out.println( Double.valueOf(fourDForm.format(89.09723483764)));
//        System.out.println("5000000 = " + getAnnualIncomeText(5000000));
    }

    private static String getAnnualIncomeText(long income) {
        String annualIncome = "Below 1 Lac";

        if (income <= 5 * LAKH && income >= LAKH) {
            annualIncome = "1-5 Lacs";
        } else if (income <= 10 * LAKH && income > 5 * LAKH) {
            annualIncome = "5-10 Lacs";
        } else if (income <= 25 * LAKH && income > 10 * LAKH) {
            annualIncome = "10-25 Lacs";
        } else if (income <= 100 * LAKH && income > 25 * LAKH) {
            annualIncome = ">25 Lacs-1 crore";
        } else if (income > 100 * LAKH) {
            annualIncome = ">1 crore";
        }
        return annualIncome;
    }

    static int gcf(Integer a, Integer b){
        BigInteger bigInteger = BigInteger.valueOf(a);
        return bigInteger.gcd(BigInteger.valueOf(b)).intValue();
    }
    static int lcmcal(int a, int b) {
        // the lcm is simply (a * b) divided by the gcf of the two
        return (a * b) / gcf(a, b);
    }


}
