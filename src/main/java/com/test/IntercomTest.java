package com.test;

import io.intercom.api.*;

import java.util.HashMap;
import java.util.Map;

/**
 *  APP ID :   xr9rs321
 *   ACCESS TOKEN  :     dG9rOmJjNzk0OTI4X2IxYzRfNDY5Nl85ZjA5XzljZmVhNzNlNzE0NToxOjA=
 *
 * Created by neeraj on 13/04/17.
 */
public class IntercomTest {

    public static void main(String...str){
        Intercom.setToken("dG9rOmJjNzk0OTI4X2IxYzRfNDY5Nl85ZjA5XzljZmVhNzNlNzE0NToxOjA=");

        getUserNotesByUserId( "7411836596");

        listAllTags();

    }

    private static void getUserNotesByUserId(String mobileNumber) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id", mobileNumber);
        NoteCollection notes = Note.list(params);
        while(notes.hasNext()) {
            System.out.println(notes.next().getBody());
        }
    }

    private static void listAllTags() {
        final TagCollection tags = Tag.list();
        while (tags.hasNext()) {
            System.out.println(tags.next());
        }
    }
}
