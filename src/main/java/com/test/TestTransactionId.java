package com.test;

import javax.swing.text.DateFormatter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by neeraj on 15/09/16.
 */
public class TestTransactionId {


    public static void main(String ... arg){
        System.out.println(generateTransactionId(null, 18L));
        System.out.println(generateTransactionId(null, 111555L));
        System.out.println(generateTransactionId(null, 11155L));
    }

    public static Long generateTransactionId(Date createdAt, Long id) {
        if (createdAt == null) {
            createdAt = new Date();
        }
        final int length = id.toString().length();
        if(length > 5){
            System.out.println(id.toString());
            id = Long.parseLong(id.toString().substring(length-5));
        }
        SimpleDateFormat s = new SimpleDateFormat("ddHH");
        System.out.println(s.format(createdAt));
//        DateFormatter fmt = DateFormat.forPattern("ddmmHH");
        return Long.parseLong(String.valueOf(createdAt.getYear() % 10)
                + s.format(createdAt)
                + shift(String.format("%05d", id), 4));
    }

    /**
     * Rotates String from breakIndex
     *
     * @param str        String
     * @param breakIndex int
     * @return String
     */
    private static String shift(String str, int breakIndex) {
        int beginIndex = 0;
        StringBuilder idStringBuilder = new StringBuilder(str.substring(beginIndex, breakIndex));
        idStringBuilder.insert(beginIndex, str.substring(breakIndex));
        return idStringBuilder.toString();
    }
}
