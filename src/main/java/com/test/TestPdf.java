package com.test;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by neeraj on 28/09/16.
 */
public class TestPdf {


    static Font fontHeader = FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
    static Font font = FontFactory.getFont(FontFactory.HELVETICA, 10, BaseColor.BLACK);
    static Font font16 = FontFactory.getFont(FontFactory.HELVETICA, 16, BaseColor.BLACK);
    static Font font16Bold = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD, BaseColor.BLACK);
    static Font fontGroww = FontFactory.getFont(FontFactory.HELVETICA, 16, new BaseColor(35, 199, 148));
    static Font fontGrowwBold = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD, new BaseColor(35, 199, 148));

    static String USER_PASS = "hello";
    static String OWNER_PASS = "owner";

    public static void main(String[] args) {
        Document document = new Document();
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("AddTableExample.pdf"));
            writer.setEncryption(USER_PASS.getBytes(), OWNER_PASS.getBytes(),
                    PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
            document.open();

            //Set attributes here
            setMetaData(document);

            addImage(document);
            addAccountStatementText(document);
            addTotalPortfolioValue(document);

            addTable(document);
            addAnyDoubtText(document);
            addAboutGrowText(document);
            addAddressText(document);



            document.close();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void setMetaData(Document document) {
        document.addAuthor("Groww");
        document.addCreationDate();
        document.addCreator("groww.in");
        document.addTitle("Monthly Statement");
        document.addSubject("Sample Monthly Statement.");
    }


    private static void addAccountStatementText(Document document) throws DocumentException {

        Paragraph p = new Paragraph();

        p.setFont(font16Bold);
        p.setSpacingBefore(5f);
        p.setSpacingAfter(5f);
        p.setIndentationLeft(150f);
        p.add("Account Statement - Sept 2016");

        document.add(p);

    }

    private static void addAnyDoubtText(Document document) throws DocumentException {

        Paragraph p = new Paragraph();

        p.setFont(font16);
        p.setSpacingBefore(100f);
        p.setSpacingAfter(5f);
        p.add("Any doubt? please write to support@groww.in");

        document.add(p);

    }

    private static void addAboutGrowText(Document document) throws DocumentException {

        Paragraph p = new Paragraph();

        p.setFont(font);
        p.setSpacingBefore(25f);
        p.setSpacingAfter(5f);
        p.setAlignment(Element.ALIGN_BOTTOM);

        p.add("About Groww: Groww is financial app run by Nextbillion Private Pvt Ltd.");

        document.add(p);

    }

    private static void addAddressText(Document document) throws DocumentException {

        Paragraph p = new Paragraph();

        p.setFont(font);
        p.setSpacingBefore(15f);
        p.setSpacingAfter(5f);
        p.setAlignment(Element.ALIGN_BOTTOM);
        p.add("Address:\n" +
                "Nextbillion Technology Pvt Ltd \n" +
                "91springboard, 4th Floor, Salarpuria Tower -1, No. 22, Industrial Layout \n" +
                "Landmark: Forum Mall, Hosur Road, Koramangala \n" +
                "Bengaluru, Karnataka\n" +
                "560095");

        document.add(p);

    }


    private static void addTable(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(5); // 3 columns.
        table.setWidthPercentage(100); //Width 100%
        table.setSpacingBefore(10f); //Space before table
        table.setSpacingAfter(10f); //Space after table

        //Set Column widths
        float[] columnWidths = {2f, 1f, 1f, 1f, 1f};
        table.setWidths(columnWidths);


        addOpeningBalanceRow(table);
        addTableHeaderRow(table);

        addTableDataRow(table);

        //To avoid having the cell border and the content overlap, if you are having thick cell borders
        //cell1.setUserBorderPadding(true);
        //cell2.setUserBorderPadding(true);
        //cell3.setUserBorderPadding(true);

//        table.addCell(cell2);
//        table.addCell(cell3);

        document.add(table);
    }

    private static void addTableHeaderRow(PdfPTable table) {

        String[] header = {"Product", "Date", "Transaction", "Amount", "Current Value"};

        for (String aHeader : header) {
            PdfPCell cell = new PdfPCell(new Phrase(aHeader, fontHeader));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setFixedHeight(25);
            cell.setBackgroundColor(new BaseColor(119, 136, 153));
            cell.setPaddingLeft(10);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.addCell(cell);

        }
    }

    private static void addOpeningBalanceRow(PdfPTable table) {

        String[] header = {"Opening Balance", "", "", "", "1000"};

        int i = 0;
        for (String aHeader : header) {
            PdfPCell cell = new PdfPCell(new Phrase(aHeader, font));
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setFixedHeight(25);
            cell.setPaddingLeft(10);
            if (i == 0) {
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            } else {
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);

            }
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.addCell(cell);
            i++;

        }
    }

    private static void addTotalPortfolioValue(Document document) throws DocumentException {

        Paragraph p = new Paragraph();

        p.setAlignment(Element.ALIGN_CENTER);
        p.setFont(fontGrowwBold);
        p.setSpacingBefore(25f);
        p.setSpacingAfter(5f);
        p.add("Rs 5053");
        document.add(p);

        p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        p.setFont(font16);
        p.add("Total Portfolio Value");
        document.add(p);

        p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        p.setFont(fontGrowwBold);
        p.setSpacingBefore(10f);
        p.setSpacingAfter(5f);
        p.add("Rs 53");
        document.add(p);

        p = new Paragraph();
        p.setAlignment(Element.ALIGN_CENTER);
        p.setFont(font16);
        p.setSpacingAfter(25f);
        p.add("Total Profit");

        document.add(p);

    }


    private static void addTableDataRow(PdfPTable table) {

        String[][] data = {{"ICICI Prudential Money Market Fund Option - Growth", "2016-09-27", "Purchase", "1000", "1012"},
                {"ICICI Prudential Flexible Income - Growth", "2016-09-27", "Purchase", "5000", "5041"}};

        int i = 0;
        for (String[] d : data) {
            for (String value : d) {
                PdfPCell cell = new PdfPCell(new Phrase(value, font));
                cell.setBorder(Rectangle.NO_BORDER);
                cell.setFixedHeight(25);
                if (i % 2 != 0) {
                    cell.setBackgroundColor(new BaseColor(211, 211, 211));
                }
                cell.setPaddingLeft(10);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                table.addCell(cell);

            }
            i++;
        }
    }


    private static void addImage(Document document) throws IOException, DocumentException {
        //Add Image
        Image image1 = Image.getInstance("src/main/java/resources/groww-logo.png");
        Anchor anchor = new Anchor();
        anchor.setReference("http://groww.in");
        image1.setAnnotation(new Annotation(0, 0, 0, 0,"http://groww.in"));
        //Fixed Positioning
//        image1.setAbsolutePosition(100f, 550f);
        image1.setAlignment(Element.ALIGN_CENTER);
        //Scale to new height and new width of image
        image1.scaleAbsolute(150, 50);
        //Add to document
        document.add(image1);

//        String imageUrl = "http://www.eclipse.org/xtend/images/java8_logo.png";
//        Image image2 = Image.getInstance(new URL(imageUrl));
//        document.add(image2);
    }


}
