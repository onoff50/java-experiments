package com.test;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import java.io.FileInputStream;

/**
 * Created by neeraj on 04/09/16.
 */
public class TestFirebase {


    public static void main(String...args) throws Exception{

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setServiceAccount(new FileInputStream("/Users/neeraj/Downloads/Groww Analytics-80e7b8e1d2e5.json"))
                .setDatabaseUrl("https://groww-analytics.firebaseio.com/")
                .build();
        FirebaseApp.initializeApp(options);


    }


}
