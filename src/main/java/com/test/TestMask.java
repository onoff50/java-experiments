package com.test;

/**
 * Created by neeraj on 03/09/16.
 */
public class TestMask {


    public static void main(String...args){

        String value = "NEW|G621352010000||1172401|11724|1001|Object reference not set to an instance of an object.|";

        int startIndex = 0;
        int endIndex = value.indexOf('|');
        System.out.println(value.substring(0, endIndex));

        startIndex = endIndex + 1;
        endIndex= value.indexOf('|', startIndex);
        System.out.println(value.substring(startIndex, endIndex));

        startIndex = endIndex + 1;
        endIndex= value.indexOf('|', startIndex);
        System.out.println(value.substring(startIndex, endIndex));

        startIndex = endIndex + 1;
        endIndex= value.indexOf('|', startIndex);
        System.out.println(value.substring(startIndex, endIndex));

        startIndex = endIndex + 1;
        endIndex= value.indexOf('|', startIndex);
        System.out.println(value.substring(startIndex, endIndex));

        startIndex = endIndex + 1;
        endIndex= value.indexOf('|', startIndex);
        System.out.println(value.substring(startIndex, endIndex));

        startIndex = endIndex + 1;
        endIndex= value.indexOf('|', startIndex);
        System.out.println(value.substring(startIndex, endIndex));

        startIndex = endIndex + 1;
        System.out.println(value.substring(startIndex));

        TestMask t = new TestMask();

        System.out.println(t.mask("546500081093", null));
        System.out.println(t.mask("546500081093", "*"));
        System.out.println(t.maskNumber("546500081093", "*******####"));
        String s = "NA";
        int length = Math.min(s.length(), 4);
        System.out.println(s.substring(s.length() - length));

    }

    public String maskNumber(String number, String mask) {

        int index = 0;
        StringBuilder masked = new StringBuilder();
        for (int i = 0; i < mask.length(); i++) {
            char c = mask.charAt(i);
            if (c == '#') {
                masked.append(number.charAt(index));
                index++;
            } else if (c == 'x') {
                masked.append(c);
                index++;
            } else {
                masked.append(c);
            }
        }
        return masked.toString();
    }


    public String mask(String number, String maskChar){
        if(maskChar == null){
            maskChar = "X";
        }
        return number.replaceAll("\\w(?=\\w{4})", maskChar);
    }


}
