package com.test;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;


public class XlsWritter {
    public static void main(String[] args) {
        try{
            String fileName = "test.xls";
            Workbook book = something(fileName);
            FileOutputStream os = new FileOutputStream(fileName);
            book.write(os);
            System.out.println("Writing on Excel file Finished ...");
            os.close();
        } catch (FileNotFoundException fe) {
            fe.printStackTrace();
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }


    private static Workbook something(String fileName) {
        Workbook myWorkbook = null;

        if (fileName.endsWith("xlsx")) {
            myWorkbook = new XSSFWorkbook();
        } else if (fileName.endsWith("xls")) {
            myWorkbook = new HSSFWorkbook();
        } else {
            throw new RuntimeException("invalid file name, should be xls or xlsx");
        }

        Sheet mySheet = myWorkbook.createSheet("Redeem" );
        Map<String, Object[]> data = new HashMap<String, Object[]>();
        data.put("7", new Object[]{7d, "Sonya", "75K", "SALES", "Rupert"});
        data.put("8", new Object[]{8d, "Kris", "85K", "SALES", "Rupert"});
        data.put("9", new Object[]{9d, "Dave", "90K", "SALES", "Rupert"});

        // Set to Iterate and add rows into XLS file
        Set<String> newRows = data.keySet();
        // get the last row number to append new data
        int rownum = mySheet.getLastRowNum();
        for (String key : newRows) { // Creating a new Row in existing XLSX sheet
            Row row = mySheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        } // open an OutputStream to save written data into XLSX file

        return myWorkbook;

    }


}
